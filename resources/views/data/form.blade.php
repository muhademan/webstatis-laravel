<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih HTML</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/login" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>

        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="negara">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysoan">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other <br> <br>

        <label>Bio:</label><br>
        <textarea cols="30" rows="10"></textarea><br>
        <button name="signup">Sign Up</button>
    </form>

</body>

</html>